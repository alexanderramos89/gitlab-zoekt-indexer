package server

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/correlation"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexer"
)

func (b DefaultIndexBuilder) getIndexDir() string {
	return b.IndexDir
}

func (b DefaultIndexBuilder) indexRepository(ctx context.Context, req indexRequest) error {
	if req.GitalyConnectionInfo == nil {
		return errors.New("gitalyConnectionInfo is not set")
	}

	if req.FileSizeLimit == 0 {
		return errors.New("fileSizeLimit is not set")
	}

	timeout, err := time.ParseDuration(req.Timeout)
	if err != nil {
		return fmt.Errorf("failed to parse Timeout: %v with error %v", req.Timeout, err)
	}

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	idx := &indexer.Indexer{
		IndexDir:           b.IndexDir,
		ProjectID:          req.RepoID,
		CorrelationID:      correlation.GetCorrelationIDFromContext(ctx),
		GitalyAddress:      req.GitalyConnectionInfo.Address,
		GitalyStorageName:  req.GitalyConnectionInfo.Storage,
		GitalyToken:        req.GitalyConnectionInfo.Token,
		GitalyRelativePath: req.GitalyConnectionInfo.Path,
		LimitFileSize:      req.FileSizeLimit,
	}

	if err := idx.IndexRepository(ctx); err != nil {
		return err
	}

	return nil
}
