package server

import (
	"context"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
)

type IndexServer struct {
	PathPrefix           string
	IndexBuilder         indexBuilder
	promRegistry         *prometheus.Registry
	metricsRequestsTotal *prometheus.CounterVec
	metricsIndexingLocks prometheus.Gauge
	IndexingLock         *indexing_lock.IndexingLock
}

type indexBuilder interface {
	getIndexDir() string
	indexRepository(ctx context.Context, req indexRequest) error
}

type DefaultIndexBuilder struct {
	IndexDir string
}

type gitalyConnectionInfo struct {
	Address string
	Token   string
	Storage string
	Path    string
}

type indexRequest struct {
	Timeout              string
	RepoID               uint32
	GitalyConnectionInfo *gitalyConnectionInfo
	FileSizeLimit        int
}
