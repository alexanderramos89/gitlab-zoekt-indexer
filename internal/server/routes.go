package server

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/correlation"
)

func (s *IndexServer) router() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Heartbeat(s.getPath("/health")))
	r.Use(correlation.Middleware)

	r.Get(s.getPath("/index/{id}"), s.handleStatus())
	r.Delete(s.getPath("/index/{id}"), s.handleDelete())
	r.Post(s.getPath("/index"), s.handleIndex())
	r.Post(s.getPath("/truncate"), s.handleTruncate())
	r.Get(s.getPath("/metrics"), s.handleMetrics())

	return r
}

func (s *IndexServer) getPath(path string) string {
	return s.PathPrefix + path
}
