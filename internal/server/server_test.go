package server

import (
	"context"
	"encoding/json"
	"io"
	"math"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/projectpath"
)

const (
	pathPrefix = "/indexer"
)

type indexBuilderMock struct {
	IndexDir string
}

func (b indexBuilderMock) indexRepository(ctx context.Context, req indexRequest) error {
	return nil
}

func (b indexBuilderMock) getIndexDir() string {
	return b.IndexDir
}

func defaultIndexServer() IndexServer {
	return IndexServer{
		PathPrefix:   pathPrefix,
		IndexingLock: indexing_lock.NewIndexingLock(&sync.Mutex{}),
	}
}

func TestCreateIndexDir(t *testing.T) {
	dirName := "tmp/test_create_index_dir_" + strconv.Itoa(rand.Intn(math.MaxInt))
	dir := filepath.Join(projectpath.Root, dirName)
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	s := IndexServer{
		IndexBuilder: indexBuilderMock{
			IndexDir: dir,
		},
	}

	s.createIndexDir()
}

func TestInitMetrics(t *testing.T) {
	server := defaultIndexServer()

	server.initMetrics()

	require.NotNil(t, server.promRegistry)

	require.NotNil(t, server.metricsRequestsTotal)

	require.NotNil(t, server.metricsIndexingLocks)
}

func TestHandleHealthCheck(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/health", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleMetrics(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/metrics", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.initMetrics()
	server.incrementRequestsTotal("GET", "/foo_bar", 200)
	server.handleMetrics()(w, req)

	resp := w.Result()

	body, _ := io.ReadAll(resp.Body)

	require.Equal(t, http.StatusOK, resp.StatusCode)
	require.Regexp(t, "gitlab_zoekt_indexer_requests_total.*foo_bar", string(body))
}

func TestHandleStatus(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/index/10", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	type statusResponse struct {
		SHA     string
		Success bool
	}

	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	var parsedResponse statusResponse
	err := dec.Decode(&parsedResponse)

	require.NoError(t, err)

	expectedResponse := statusResponse{
		SHA:     "5f6ffd6461ba03e257d24eed4f1f33a7ee3c2784",
		Success: true,
	}

	if diff := cmp.Diff(expectedResponse, parsedResponse); diff != "" {
		t.Error(diff)
	}
}

func TestHandleStatusWhenNotFound(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/index/0", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()
	require.Equal(t, http.StatusNotFound, resp.StatusCode)
}

func TestHandleTruncate(t *testing.T) {
	req := httptest.NewRequest("POST", "/indexer/truncate", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/truncate_test")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	testFilePath := filepath.Join(dir, "test_file.txt")
	data := []byte("hello world")
	err := os.WriteFile(testFilePath, data, 0644)

	require.NoError(t, err)

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	if _, err := os.Stat(testFilePath); err == nil {
		t.Fatalf("file %v still exists", testFilePath)
	}
}

func TestHandleDelete(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/indexer/index/7", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/delete_test")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}

	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	fileToBeDeleted1 := "7_v16.00000.zoekt"
	fileToBeDeleted2 := "7_v16.00001.zoekt"
	fileToKeep1 := "700_v16.00000.zoekt"
	fileToKeep2 := "707_v16.00000.zoekt"
	testFiles := []string{fileToBeDeleted1, fileToBeDeleted2, fileToKeep1, fileToKeep2}

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, []string{fileToKeep1, fileToKeep2}, existingFiles)
}

func TestHandleIndex(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleIndexWhenErr(t *testing.T) {
	r := strings.NewReader(`{"nonExistingField": true}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.IndexBuilder = indexBuilderMock{
		IndexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestHandleIndexWhenTimeoutErr(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.IndexBuilder = DefaultIndexBuilder{
		IndexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	type response struct {
		Success bool
		Error   string
	}

	var parsedResp response
	err := json.NewDecoder(resp.Body).Decode(&parsedResp)

	require.NoError(t, err)
	require.Equal(t, http.StatusGatewayTimeout, resp.StatusCode)
	require.Equal(t, "context deadline exceeded", parsedResp.Error)
}
