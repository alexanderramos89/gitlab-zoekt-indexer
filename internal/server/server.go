package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexer"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/tmp_cleaner"
)

func (s *IndexServer) StartIndexingApi(listen string) {
	s.initMetrics()
	s.createIndexDir()
	s.setUpTmpCleaner()

	httpServer := http.Server{
		Addr:    listen,
		Handler: s.router(),
	}

	log.Printf("Starting server on %s with '%s' as path prefix", listen, s.PathPrefix)

	if err := httpServer.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func (s *IndexServer) createIndexDir() {
	if err := os.MkdirAll(s.IndexBuilder.getIndexDir(), 0o755); err != nil {
		log.Fatalf("createIndexDir %s: %v", s.IndexBuilder.getIndexDir(), err)
	}
}

func (s *IndexServer) setUpTmpCleaner() {
	tmpCleaner := tmp_cleaner.TmpCleaner{
		IndexDir:     s.IndexBuilder.getIndexDir(),
		IndexingLock: s.IndexingLock,
	}

	tmpCleaner.StartCleanInterval(time.NewTicker(5 * time.Minute))
}

func (s *IndexServer) handleStatus() http.HandlerFunc {
	route := "status"

	type response struct {
		Success bool
		SHA     string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		param := chi.URLParam(r, "id")
		repoID, err := strconv.ParseUint(param, 10, 32)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		idx := &indexer.Indexer{
			IndexDir:  s.IndexBuilder.getIndexDir(),
			ProjectID: uint32(repoID),
		}

		currentSHA, ok, err := idx.CurrentSHA()

		if err != nil {
			s.respondWithError(w, r, route, err, http.StatusInternalServerError)
			return
		}

		if !ok {
			s.respondWithStatus(w, r, route, http.StatusNotFound)
			return
		}

		resp := response{
			Success: true,
			SHA:     currentSHA,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *IndexServer) handleDelete() http.HandlerFunc {
	route := "delete"

	removeShards := func(dir string, repoID uint64) error {
		getRepoFiles := func() ([]string, error) {
			// repoID should always be an integer
			glob := fmt.Sprintf("%d_*.zoekt", repoID)
			path := filepath.Join(dir, glob)
			files, err := filepath.Glob(path)

			if err != nil {
				return nil, err
			}

			return files, nil
		}

		files, err := getRepoFiles()

		if err != nil {
			return err
		}

		for _, file := range files {
			if err := os.Remove(file); err != nil {
				return err
			}
		}

		return nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		param := chi.URLParam(r, "id")
		repoID, err := strconv.ParseUint(param, 10, 32)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := removeShards(s.IndexBuilder.getIndexDir(), repoID); err != nil {
			err = fmt.Errorf("failed to remove shards for repoID: %d from %v with error: %v", repoID, s.IndexBuilder.getIndexDir(), err)

			s.respondWithError(w, r, route, err, http.StatusInternalServerError)
			return
		}

		resp := struct {
			Success bool
		}{
			Success: true,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *IndexServer) handleMetrics() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.updateIndexingLockGauge()
		promhttp.HandlerFor(s.promRegistry, promhttp.HandlerOpts{Registry: s.promRegistry}).ServeHTTP(w, r)
	}
}

func (s *IndexServer) decode(r *http.Request, v interface{}) error {
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	return dec.Decode(v)
}

func (s *IndexServer) handleIndex() http.HandlerFunc {
	route := "index"

	type response struct {
		Success bool
	}

	parseRequest := func(r *http.Request) (indexRequest, error) {
		var req indexRequest
		err := s.decode(r, &req)

		if err != nil {
			return req, errors.New("json parser error")
		}

		return req, nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req, err := parseRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if s.IndexingLock.IsLocked(req.RepoID) {
			s.respondWithError(w, r, route, errors.New("indexing is already in progress"), http.StatusLocked)

			return
		}

		s.IndexingLock.Set(req.RepoID, true)
		defer s.IndexingLock.Set(req.RepoID, false)

		err = s.IndexBuilder.indexRepository(r.Context(), req)

		if err != nil {
			s.respondWithError(w, r, route, err, http.StatusInternalServerError)
			return
		}

		resp := response{
			Success: true,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *IndexServer) handleTruncate() http.HandlerFunc {
	route := "truncate"

	type response struct {
		Success bool
	}

	emptyDirectory := func(dir string) error {
		files, err := os.ReadDir(dir)

		if err != nil {
			return err
		}

		for _, file := range files {
			filePath := filepath.Join(dir, file.Name())
			err := os.RemoveAll(filePath)
			if err != nil {
				return err
			}
		}

		return nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		err := emptyDirectory(s.IndexBuilder.getIndexDir())

		if err != nil {
			err = fmt.Errorf("failed to empty indexDir: %v with error: %v", s.IndexBuilder.getIndexDir(), err)

			s.respondWithError(w, r, route, err, http.StatusInternalServerError)
			return
		}

		resp := response{
			Success: true,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *IndexServer) respondWith(w http.ResponseWriter, r *http.Request, route string, data interface{}) {
	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(data); err != nil {
		s.respondWithError(w, r, route, err, http.StatusInternalServerError)
		return
	}

	s.incrementRequestsTotal(r.Method, route, http.StatusOK)
}

func (s *IndexServer) respondWithError(w http.ResponseWriter, r *http.Request, route string, err error, responseCode int) {
	type response struct {
		Success bool
		Error   string
	}

	if errors.Is(err, context.DeadlineExceeded) {
		responseCode = http.StatusGatewayTimeout
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseCode)

	resp := response{
		Success: false,
		Error:   err.Error(),
	}

	_ = json.NewEncoder(w).Encode(resp)

	s.incrementRequestsTotal(r.Method, route, responseCode)
}

func (s *IndexServer) respondWithStatus(w http.ResponseWriter, r *http.Request, route string, responseCode int) {
	w.WriteHeader(responseCode)
	s.incrementRequestsTotal(r.Method, route, responseCode)
}

func (s *IndexServer) incrementRequestsTotal(method, route string, responseCode int) {
	s.metricsRequestsTotal.With(prometheus.Labels{"code": strconv.Itoa(responseCode), "method": method, "route": route}).Inc()
}

func (s *IndexServer) updateIndexingLockGauge() {
	n := float64(len(s.IndexingLock.InProgress()))
	s.metricsIndexingLocks.Set(n)
}

func (s *IndexServer) initMetrics() {
	s.promRegistry = prometheus.NewRegistry()

	// Add go runtime metrics and process collectors.
	s.promRegistry.MustRegister(
		collectors.NewGoCollector(),
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
	)

	s.metricsRequestsTotal = promauto.With(s.promRegistry).NewCounterVec(
		prometheus.CounterOpts{
			Name: "gitlab_zoekt_indexer_requests_total",
			Help: "Total number of HTTP requests by status code, method, and route.",
		},
		[]string{"method", "route", "code"},
	)
	s.metricsIndexingLocks = promauto.With(s.promRegistry).NewGauge(
		prometheus.GaugeOpts{
			Name: "gitlab_zoekt_indexing_locks",
			Help: "Number of indexing locks currently in progress",
		},
	)
}
