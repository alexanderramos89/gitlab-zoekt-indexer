package correlation

import (
	"context"
	"net/http"

	corr "gitlab.com/gitlab-org/labkit/correlation"
)

func Middleware(next http.Handler) http.Handler {
	return corr.InjectCorrelationID(next, corr.WithPropagation())
}

func GetCorrelationIDFromContext(ctx context.Context) string {
	return corr.ExtractFromContextOrGenerate(ctx)
}
