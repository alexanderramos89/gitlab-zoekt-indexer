package indexing_lock

import "sync"

type IndexingLock struct {
	state      sync.Locker
	inProgress map[uint32]lockStructValue
}

type lockStructValue struct{}

func NewIndexingLock(l sync.Locker) *IndexingLock {
	return &IndexingLock{
		state:      l,
		inProgress: make(map[uint32]lockStructValue),
	}
}

func (lock *IndexingLock) InProgress() []uint32 {
	keys := make([]uint32, len(lock.inProgress))
	i := 0
	for k := range lock.inProgress {
		keys[i] = k
		i++
	}

	return keys
}

func (lock *IndexingLock) Lock() {
	lock.state.Lock()
}

func (lock *IndexingLock) Unlock() {
	lock.state.Unlock()
}

func (lock *IndexingLock) IsLocked(repoID uint32) bool {
	_, ok := lock.inProgress[repoID]

	return ok
}

func (lock *IndexingLock) Set(repoID uint32, flag bool) {
	lock.state.Lock()
	defer lock.state.Unlock()

	if flag {
		lock.inProgress[repoID] = lockStructValue{}
	} else {
		delete(lock.inProgress, repoID)
	}
}
