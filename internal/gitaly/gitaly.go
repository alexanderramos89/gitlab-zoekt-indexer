package gitaly

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"

	gitalyauth "gitlab.com/gitlab-org/gitaly/v16/auth"
	gitalyclient "gitlab.com/gitlab-org/gitaly/v16/client"
	pb "gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb"
	"gitlab.com/gitlab-org/labkit/correlation"
	grpccorrelation "gitlab.com/gitlab-org/labkit/correlation/grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	NullTreeSHA       = "4b825dc642cb6eb9a060e54bf8d69288fbee4904" // SHA1("tree 0\0")
	ZeroSHA           = "0000000000000000000000000000000000000000"
	ClientName        = "gitlab-zoekt-indexer"
	SubmoduleFileMode = 0160000
)

type StorageConfig struct {
	Address      string `json:"address"`
	Token        string `json:"token"`
	StorageName  string `json:"storage"`
	RelativePath string `json:"relative_path"`
	ProjectPath  string `json:"project_path"`
	TokenVersion int    `json:"token_version"`
}

type GitalyClient struct {
	conn                    *grpc.ClientConn
	repository              *pb.Repository
	blobServiceClient       pb.BlobServiceClient
	repositoryServiceClient pb.RepositoryServiceClient
	refServiceClient        pb.RefServiceClient
	commitServiceClient     pb.CommitServiceClient
	ctx                     context.Context
	FromHash                string
	ToHash                  string
	limitFileSize           int64
}

type File struct {
	Path     string
	Blob     func() (io.ReadCloser, error)
	Oid      string
	Size     int64
	TooLarge bool
}

func (file *File) BlobContent() ([]byte, error) {
	r, err := file.Blob()
	if err != nil {
		return nil, err
	}
	defer r.Close()

	var buf bytes.Buffer
	buf.Grow(int(file.Size))
	_, err = buf.ReadFrom(r)

	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type PutFunc func(file *File) error
type DelFunc func(path string) error

func NewGitalyClient(config *StorageConfig, correlationID string, projectID uint32, limitFileSize int64) (*GitalyClient, error) {
	var RPCCred credentials.PerRPCCredentials
	if config.TokenVersion == 0 || config.TokenVersion == 2 {
		RPCCred = gitalyauth.RPCCredentialsV2(config.Token)
	} else {
		return nil, errors.New("unknown token version")
	}

	connOpts := append(
		gitalyclient.DefaultDialOpts,
		grpc.WithPerRPCCredentials(RPCCred),
		grpc.WithStreamInterceptor(
			grpccorrelation.StreamClientCorrelationInterceptor(
				grpccorrelation.WithClientName(ClientName),
			),
		),
		grpc.WithUnaryInterceptor(
			grpccorrelation.UnaryClientCorrelationInterceptor(
				grpccorrelation.WithClientName(ClientName),
			),
		),
	)

	ctx := newContext(correlationID)

	conn, err := gitalyclient.Dial(config.Address, connOpts)
	if err != nil {
		return nil, fmt.Errorf("did not connect: %s", err)
	}

	repository := &pb.Repository{
		StorageName:   config.StorageName,
		RelativePath:  config.RelativePath,
		GlProjectPath: config.ProjectPath,
		GlRepository:  fmt.Sprint(projectID),
	}

	client := &GitalyClient{
		conn:                    conn,
		repository:              repository,
		blobServiceClient:       pb.NewBlobServiceClient(conn),
		repositoryServiceClient: pb.NewRepositoryServiceClient(conn),
		refServiceClient:        pb.NewRefServiceClient(conn),
		commitServiceClient:     pb.NewCommitServiceClient(conn),
		limitFileSize:           limitFileSize,
		ctx:                     ctx,
	}

	return client, nil
}

func (gc *GitalyClient) Close() {
	gc.conn.Close()
}

func (gc *GitalyClient) IsValidSHA(SHA string) bool {
	request := &pb.FindCommitRequest{
		Repository: gc.repository,
		Revision:   []byte(SHA),
	}

	_, err := gc.commitServiceClient.FindCommit(gc.ctx, request)
	return err == nil
}

func (gc *GitalyClient) GetCurrentSHA() (string, error) {
	defaultBranchName, err := gc.findDefaultBranchName()

	if err != nil {
		return "", err
	}

	request := &pb.FindCommitRequest{
		Repository: gc.repository,
		Revision:   defaultBranchName,
	}

	response, err := gc.commitServiceClient.FindCommit(gc.ctx, request)
	if err != nil {
		return "", fmt.Errorf("cannot look up HEAD: %v", err)
	}
	return response.Commit.Id, nil
}

func (gc *GitalyClient) findDefaultBranchName() ([]byte, error) {
	request := &pb.FindDefaultBranchNameRequest{
		Repository: gc.repository,
	}

	response, err := gc.refServiceClient.FindDefaultBranchName(gc.ctx, request)
	if err != nil {
		return nil, fmt.Errorf("cannot find a default branch: %v", err)
	}
	return response.Name, nil
}

func (gc *GitalyClient) EachFileChange(put PutFunc, del DelFunc) error {
	if gc.FromHash == "" || gc.FromHash == ZeroSHA {
		gc.FromHash = NullTreeSHA
	}

	request := &pb.GetRawChangesRequest{
		Repository:   gc.repository,
		FromRevision: gc.FromHash,
		ToRevision:   gc.ToHash,
	}

	stream, err := gc.repositoryServiceClient.GetRawChanges(gc.ctx, request)
	if err != nil {
		return fmt.Errorf("could not call rpc.GetRawChanges: %v", err)
	}

	for {
		c, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("%v.GetRawChanges, %v", c, err)
		}
		for _, change := range c.RawChanges {
			// We skip submodules from indexing now just to mirror the go-git
			// implementation but it can be not that expensive to implement with gitaly actually so some
			// investigation is required here
			if change.OldMode == SubmoduleFileMode || change.NewMode == SubmoduleFileMode {
				continue
			}

			switch change.Operation.String() {
			case "DELETED", "RENAMED":
				path := string(change.OldPathBytes)
				if err = del(path); err != nil {
					return err
				}
			}

			switch change.Operation.String() {
			case "ADDED", "RENAMED", "MODIFIED", "COPIED":
				file, err := gc.gitalyBuildFile(change, string(change.NewPathBytes))
				if err != nil {
					return err
				}
				if err = put(file); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (gc *GitalyClient) gitalyBuildFile(change *pb.GetRawChangesResponse_RawChange, path string) (*File, error) {
	var data io.ReadCloser
	var err error
	data, err = gc.getBlob(change.BlobId)
	if err != nil {
		return nil, fmt.Errorf("getBlob returns error: %v", err)
	}

	tooLarge := change.Size > gc.limitFileSize
	return &File{
		Path:     path,
		Oid:      change.BlobId,
		Blob:     getBlobReader(data),
		Size:     change.Size,
		TooLarge: tooLarge,
	}, nil
}

func (gc *GitalyClient) getBlob(oid string) (io.ReadCloser, error) {
	data := new(bytes.Buffer)

	request := &pb.GetBlobRequest{
		Repository: gc.repository,
		Oid:        oid,
		Limit:      gc.limitFileSize,
	}

	stream, err := gc.blobServiceClient.GetBlob(gc.ctx, request)
	if err != nil {
		return nil, fmt.Errorf("cannot get blob: %s", oid)
	}

	for {
		c, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("%v.GetBlob: %v", c, err)
		}
		if c.Data != nil {
			data.Write(c.Data)
		}
	}

	return io.NopCloser(data), nil
}

func getBlobReader(data io.ReadCloser) func() (io.ReadCloser, error) {
	return func() (io.ReadCloser, error) { return data, nil }
}

func newContext(correlationID string) context.Context {
	return correlation.ContextWithCorrelation(context.Background(), correlationID)
}
