package indexer

import (
	"context"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/gitaly"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/zoekt"
)

type Indexer struct {
	IndexDir           string
	ProjectID          uint32
	CorrelationID      string
	GitalyAddress      string
	GitalyToken        string
	GitalyStorageName  string
	GitalyRelativePath string
	gitalyClient       *gitaly.GitalyClient
	zoektClient        *zoekt.Client
	LimitFileSize      int
}

func (i *Indexer) initClients() error {
	config := &gitaly.StorageConfig{
		Address:      i.GitalyAddress,
		Token:        i.GitalyToken,
		StorageName:  i.GitalyStorageName,
		RelativePath: i.GitalyRelativePath,
	}
	gitalyClient, err := gitaly.NewGitalyClient(config, i.CorrelationID, i.ProjectID, int64(i.LimitFileSize))

	if err != nil {
		return err
	}

	i.gitalyClient = gitalyClient

	gitalySHA, err := i.gitalyClient.GetCurrentSHA()
	if err != nil {
		return err
	}

	i.initZoektClient(gitalySHA)

	return nil
}

func (i *Indexer) initZoektClient(gitalySHA string) {
	branches := []zoekt.RepositoryBranch{
		{
			Name:    "HEAD",
			Version: gitalySHA,
		},
	}

	zoektOptions := &zoekt.Options{
		IndexDir: i.IndexDir,
		ID:       i.ProjectID,
		SizeMax:  i.LimitFileSize,
		Branches: branches,
	}

	i.zoektClient = zoekt.NewZoektClient(zoektOptions)
}

func (i *Indexer) CurrentSHA() (string, bool, error) {
	i.initZoektClient("")

	zoektSHA, ok, err := i.zoektClient.GetCurrentSHA()

	if err != nil {
		return "", ok, err
	}

	return zoektSHA, ok, nil
}

func (i *Indexer) IndexRepository(ctx context.Context) error {
	if err := ctx.Err(); err != nil {
		return err
	}

	if err := i.initClients(); err != nil {
		return err
	}
	defer i.gitalyClient.Close()

	skipIndexing := i.zoektClient.IncrementalSkipIndexing()

	if skipIndexing {
		return nil
	}

	gitalySHA, gitalyErr := i.gitalyClient.GetCurrentSHA()

	if gitalyErr != nil {
		return gitalyErr
	}

	builder, builderErr := i.zoektClient.NewBuilder()
	if builderErr != nil {
		return builderErr
	}

	putFunc := func(file *gitaly.File) error {
		if err := ctx.Err(); err != nil {
			return err
		}

		content, e := file.BlobContent()
		if e != nil {
			return e
		}

		builder.MarkFileAsChangedOrRemoved(file.Path)
		if e := i.zoektClient.AddFile(builder, file.Path, content, file.Size, []string{"HEAD"}); e != nil {
			return e
		}

		return nil
	}

	delFunc := func(path string) error {
		if err := ctx.Err(); err != nil {
			return err
		}

		builder.MarkFileAsChangedOrRemoved(path)
		return nil
	}

	i.gitalyClient.ToHash = gitalySHA
	zoektSHA, ok, err := i.CurrentSHA()

	if err != nil {
		return err
	}

	if ok && i.gitalyClient.IsValidSHA(zoektSHA) {
		i.gitalyClient.FromHash = zoektSHA
	} else {
		i.gitalyClient.FromHash = ""
	}

	if err := i.gitalyClient.EachFileChange(putFunc, delFunc); err != nil {
		return err
	}

	if err := builder.Finish(); err != nil {
		return err
	}

	return nil
}
