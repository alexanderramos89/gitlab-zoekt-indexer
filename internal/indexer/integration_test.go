package indexer

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"

	gitalyauth "gitlab.com/gitlab-org/gitaly/v16/auth"
	gitalyClient "gitlab.com/gitlab-org/gitaly/v16/client"
	pb "gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/projectpath"
)

var (
	gitalyConnInfo              *gitalyConnectionInfo
	gitalyRepositoryInitialized bool
)

const (
	headSHA           = "b83d6e391c22777fca1ed3012fce84f633d7fed0"
	projectID         = 72724
	testRepo          = "gitlab-org/gitlab-test.git"
	testProjectPath   = "gitlab-org/gitlab-test"
	testRepoPath      = "https://gitlab.com/gitlab-org/gitlab-test.git"
	testRepoNamespace = "gitlab-org"
)

type gitalyConnectionInfo struct {
	Address string `json:"address"`
	Storage string `json:"storage"`
	Token   string `json:"token"`
}

func TestMain(m *testing.M) {
	integration := os.Getenv("INTEGRATION")
	if integration != "" && integration != "false" {
		m.Run()
	}
}

func init() {
	gci, exists := os.LookupEnv("GITALY_CONNECTION_INFO")
	if exists {
		err := json.Unmarshal([]byte(gci), &gitalyConnInfo)
		if err != nil {
			panic(err)
		}
	} else {
		gitalyConnInfo = &gitalyConnectionInfo{
			Address: "tcp://localhost:8075",
			Storage: "default",
		}
	}
}

func ensureGitalyRepository(t *testing.T) error {
	if gitalyRepositoryInitialized {
		return nil
	}

	RPCCred := gitalyauth.RPCCredentialsV2(gitalyConnInfo.Token)

	connOpts := append(
		gitalyClient.DefaultDialOpts,
		grpc.WithPerRPCCredentials(RPCCred),
	)

	conn, err := gitalyClient.Dial(gitalyConnInfo.Address, connOpts)

	if err != nil {
		return fmt.Errorf("did not connect: %s", err)
	}

	namespace := pb.NewNamespaceServiceClient(conn)

	repository := pb.NewRepositoryServiceClient(conn)

	// Remove the repository if it already exists, for consistency
	rmNsReq := &pb.RemoveNamespaceRequest{
		StorageName: gitalyConnInfo.Storage,
		Name:        testRepoNamespace,
	}
	_, err = namespace.RemoveNamespace(context.Background(), rmNsReq)
	if err != nil {
		return err
	}

	gl_repository := &pb.Repository{
		StorageName:   gitalyConnInfo.Storage,
		RelativePath:  testRepo,
		GlProjectPath: testProjectPath,
	}

	createReq := &pb.CreateRepositoryFromURLRequest{
		Repository: gl_repository,
		Url:        testRepoPath,
	}

	_, err = repository.CreateRepositoryFromURL(context.Background(), createReq)
	if err != nil {
		return err
	}

	writeHeadReq := &pb.WriteRefRequest{
		Repository: gl_repository,
		Ref:        []byte("refs/heads/master"),
		Revision:   []byte("b83d6e391c22777fca1ed3012fce84f633d7fed0"),
	}

	_, err = repository.WriteRef(context.Background(), writeHeadReq)

	if err == nil {
		gitalyRepositoryInitialized = true
	}

	return err
}

func TestSetupRepository(t *testing.T) {
	require.NoError(t, ensureGitalyRepository(t))
}

func TestInitClients(t *testing.T) {
	require.NoError(t, ensureGitalyRepository(t))

	indexer := Indexer{
		IndexDir:           filepath.Join(projectpath.Root, "_support/test/shards"),
		ProjectID:          projectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: testRepo,
	}

	err := indexer.initClients()

	require.NoError(t, err)
}

func TestIndexRepository(t *testing.T) {
	require.NoError(t, ensureGitalyRepository(t))

	indexDir := filepath.Join(projectpath.Root, "tmp/indexer_test")
	os.RemoveAll(indexDir) // Clean directory before test runs
	defer os.RemoveAll(indexDir)

	indexer := Indexer{
		IndexDir:           indexDir,
		ProjectID:          projectID,
		GitalyAddress:      gitalyConnInfo.Address,
		GitalyStorageName:  gitalyConnInfo.Storage,
		GitalyToken:        gitalyConnInfo.Token,
		GitalyRelativePath: testRepo,
	}

	err := indexer.IndexRepository(context.TODO())

	require.NoError(t, err)

	sha, _, err := indexer.CurrentSHA()

	require.NoError(t, err)
	require.Equal(t, headSHA, sha)
}
