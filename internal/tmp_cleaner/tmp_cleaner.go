package tmp_cleaner

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
)

var (
	IDRegex = regexp.MustCompile(`^\d+`)
)

type TmpCleaner struct {
	IndexDir     string
	Ticker       *time.Ticker
	IndexingLock *indexing_lock.IndexingLock
}

func (c *TmpCleaner) StartCleanInterval(t *time.Ticker) {
	go func() {
		for range t.C {
			if err := c.cleanTmpFiles(); err != nil {
				fmt.Printf("%+v\n", err)
			}
		}
	}()
}

func (c *TmpCleaner) cleanTmpFiles() error {
	c.IndexingLock.Lock()
	defer c.IndexingLock.Unlock()

	files, err := c.getTmpFiles()

	if err != nil {
		return err
	}

	return c.deleteFiles(files)
}

func (c *TmpCleaner) getTmpFiles() ([]string, error) {
	dir := c.IndexDir
	glob := "*.tmp"
	path := filepath.Join(dir, glob)
	files, err := filepath.Glob(path)

	if err != nil {
		return nil, err
	}

	filteredFiles, err := c.filterInProgress(files)

	if err != nil {
		return nil, err
	}

	return filteredFiles, nil
}

func (c *TmpCleaner) deleteFiles(files []string) error {
	for _, file := range files {
		if err := os.Remove(file); err != nil {
			return err
		}
	}

	return nil
}

func (c *TmpCleaner) filterInProgress(files []string) ([]string, error) {
	result := make([]string, 0, len(files))
	for _, file := range files {
		fileName := filepath.Base(file)

		repoID := IDRegex.FindString(fileName)

		if repoID == "" {
			fmt.Printf("failed to find repoID for %v\n", fileName)
			continue
		}

		rID, err := strconv.ParseUint(repoID, 10, 32)

		if err != nil {
			return nil, err
		}

		if !c.IndexingLock.IsLocked(uint32(rID)) {
			result = append(result, file)
		}
	}

	return result, nil
}
