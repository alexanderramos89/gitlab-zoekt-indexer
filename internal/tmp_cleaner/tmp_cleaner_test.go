package tmp_cleaner

import (
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/projectpath"
)

func TestCleanTmpFiles(t *testing.T) {
	dir := filepath.Join(projectpath.Root, "tmp/tmp_files")

	tmpCleaner := TmpCleaner{
		IndexDir:     dir,
		Ticker:       time.NewTicker(10 * time.Minute),
		IndexingLock: indexing_lock.NewIndexingLock(&sync.Mutex{}),
	}

	if err := os.MkdirAll(dir, 0o755); err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	tmpCleaner.IndexingLock.Set(7, true)

	fileToBeDeleted1 := "700_v16.00703.zoekt.3115102821.tmp"
	fileToBeDeleted2 := "707_v16.00703.zoekt.3115102821.tmp"
	fileToKeep1 := "700_v16.00000.zoekt"
	fileToKeep2 := "707_v16.00000.zoekt"
	fileToKeep3 := "7_v16.00702.zoekt.3269846088.tmp"
	testFiles := []string{fileToBeDeleted1, fileToBeDeleted2, fileToKeep1, fileToKeep2, fileToKeep3}

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		data := []byte("hello world")
		err := os.WriteFile(testFilePath, data, 0644)

		require.NoError(t, err)
	}

	err := tmpCleaner.cleanTmpFiles()

	require.NoError(t, err)

	var existingFiles []string

	for _, file := range testFiles {
		testFilePath := filepath.Join(dir, file)
		if _, err := os.Stat(testFilePath); err == nil {
			existingFiles = append(existingFiles, file)
		}
	}

	require.Equal(t, []string{fileToKeep1, fileToKeep2, fileToKeep3}, existingFiles)
}
