.DEFAULT_GOAL := build
INTEGRATION ?= true
export INTEGRATION

listen ?= :6060
index_dir ?= ./tmp/indexer

test:
	@echo "Executing tests with INTEGRATION set to '$(INTEGRATION)'"
	go test ./... -race
.PHONY:test

cover:
	@echo "Executing tests with INTEGRATION set to '$(INTEGRATION)'"
	go test ./... -race -cover -coverprofile=tmp/test.coverage
	gcov2lcov -infile=tmp/test.coverage -outfile=tmp/coverage.xml
.PHONY:cover

build:
	go build $(VERSION_FLAGS) -o bin/gitlab-zoekt-indexer ./cmd/gitlab-zoekt-indexer/
.PHONY:build

run: build
	./bin/gitlab-zoekt-indexer -index_dir $(index_dir) -listen $(listen)
.PHONY:run

watch-run:
	watchexec -c -n -r -e go -- make run
.PHONY:watch-run

watch-test:
	watchexec -c -p -n -e go -- make test
.PHONY:watch-test

watch-cover:
	watchexec -c -p -n -e go -- make cover
.PHONY:watch-cover

##### =====> Internals <===== #####

DATE             := $(shell date -u '+%Y.%m.%d')
VERSION          := $(shell git describe --tags --always --dirty=".dev")
BUILD_TIME       := $(shell date -u '+%Y-%m-%d %H:%M UTC')
VERSION_FLAGS    := -ldflags='-X "main.Version=$(DATE)-$(VERSION)" -X "main.BuildTime=$(BUILD_TIME)"'