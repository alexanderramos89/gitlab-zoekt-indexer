package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"

	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexing_lock"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/server"
)

var (
	// Overriden in the makefile
	Version   = "dev"
	BuildTime = ""
)

type options struct {
	indexDir   string
	pathPrefix string
	listen     string
}

func parseOptions() options {
	versionFlag := flag.Bool("version", false, "Print the version and exit")
	indexDir := flag.String("index_dir", "", "directory holding index shards.")
	pathPrefix := flag.String("path_prefix", "/indexer", "prefix for the routes")
	listen := flag.String("listen", ":6060", "listen on this address.")

	flag.Parse()

	if *versionFlag {
		fmt.Fprintf(os.Stdout, "%s %s (built at: %s)\n", os.Args[0], Version, BuildTime)
		os.Exit(0)
	}

	if *indexDir == "" {
		log.Printf("Usage: %s [ --version | --index_dir=<DIR> | --path_prefix=<PREFIX> | --listen=:<PORT> ]\n", os.Args[0])
		log.Fatalln("Must set -index_dir")
	}

	return options{
		indexDir:   *indexDir,
		pathPrefix: *pathPrefix,
		listen:     *listen,
	}
}

func main() {
	opts := parseOptions()

	s := server.IndexServer{
		PathPrefix: opts.pathPrefix,
		IndexBuilder: server.DefaultIndexBuilder{
			IndexDir: opts.indexDir,
		},
		IndexingLock: indexing_lock.NewIndexingLock(&sync.Mutex{}),
	}

	s.StartIndexingApi(opts.listen)
}
